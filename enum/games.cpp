#include "games.h"

Games   determineGame(QString game){
    if(game == TWO_CAR_GAME)
        return TwoCar;
    else if(game == SPACESHIP_GAME)
        return SpaceShip;

    return None;
}
