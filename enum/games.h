#ifndef GAMES_H
#define GAMES_H

#include <QString>

#define TWO_CAR_GAME    "1"
#define SPACESHIP_GAME  "2"

enum Games
{  None = 0 , TwoCar = 1 , SpaceShip = 2 };

Games   determineGame(QString game);

#endif // GAMES_H

