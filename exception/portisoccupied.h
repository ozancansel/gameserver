#ifndef PORTISOCCUPIED_H
#define PORTISOCCUPIED_H

#include <QException>
#include <QDebug>

class PortIsOccupied : public  QException
{

public:

    PortIsOccupied(int port);
    void raise() const {
        qDebug() << "Error : " << QString::number(_port) <<  " port is occupied!";
        throw *this;
    }
    PortIsOccupied *clone() const { return new PortIsOccupied(*this); }

private:

    int     _port;

};

#endif // PORTISOCCUPIED_H
