#ifndef USER_H
#define USER_H

#include <QObject>
#include <QWebSocket>
#include "enum/games.h"

#define   ADD_POOL_COMMAND      "1"
#define   CHANGE_STATE          "2"
#define   SET_NAME              "3"
#define   IDENTIFY_COMMAND      "4"
#define   INFORM_SPAWNED_GAME   "5"

class Client : public QObject
{

    Q_OBJECT

public:

    enum        State{ NotReady = 0, READY = 1};
    Client(QWebSocket *socket , QObject *parent = Q_NULLPTR);
    QWebSocket* socket();
    QString     toString();
    State       state();
    QString     name();
    void        informSpawnedGame(QString   address);
    bool        isAuthorized();

signals:

    void    addMatchMaking(Games game);
    void    onNewMessageReceived(const QString &code , const QStringList &args);
    void    authorized();

public slots:

    void    identify();
    void    setNameTimeout();

private slots   :

    void    onTextMessageReceived(const QString &message);

private :

    QWebSocket                  *_socket;
    QHash<QString , Games>      _stringToGameMap;
    State                       _state;
    QString                     _name;

    void                        changeState(QStringList &args);
    void                        setName(QStringList &args);
    void                        addPool(QStringList &args);

};

#endif // USER_H
