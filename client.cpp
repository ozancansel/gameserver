#include "client.h"
#include <QTimer>

Client::Client(QWebSocket *socket , QObject *parent) : QObject(parent){
    _socket = socket;

    connect(_socket , SIGNAL(textMessageReceived(QString)) , SLOT(onTextMessageReceived(QString)));
    QTimer::singleShot(1000 , this , SLOT(setNameTimeout()));
}

QWebSocket*     Client::socket(){
    return _socket;
}

bool Client::isAuthorized(){
    return !_name.isEmpty();
}

QString Client::toString(){
    return _name;
}

void Client::onTextMessageReceived(const QString &message){

    qDebug() << "Message received => " << message;
    QStringList commandList = message.split("|");

    foreach (QString message, commandList) {
        QStringList args = message.split(",");
        QString     code = args.at(0);
        args.removeAt(0);

        //Not authorized region

        if(code == SET_NAME)
            setName(args);

        //Giris yapilmamissa geri donuluyor
        if(!isAuthorized()){
            identify();
            return;
        }

        //Authorized region

        if(code == ADD_POOL_COMMAND)
            addPool(args);
        else if(code == CHANGE_STATE)
            changeState(args);
    }

}

void Client::setNameTimeout(){
    //Not implemented
}

void Client::identify(){
    _socket->sendTextMessage(IDENTIFY_COMMAND);
}

Client::State Client::state(){
    return  _state;
}

QString Client::name(){
    return _name;
}

//Commands
void Client::addPool(QStringList &args){
    QString gameName = args.at(0);
    Games   game = determineGame(gameName);

    emit addMatchMaking(game);
}

void Client::changeState(QStringList &args){
    //Not implemented
}

void Client::setName(QStringList &args){
    QString     name = args.at(0);

    _name = name;
    emit authorized();
}

void Client::informSpawnedGame(QString address){
    socket()->sendTextMessage(QString(INFORM_SPAWNED_GAME).append(",").append(address));
}
