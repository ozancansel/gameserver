#ifndef MATCHMAKER_H
#define MATCHMAKER_H

#include <QQueue>
#include "client.h"
#include "spawn/gamespawn.h"

class Matchmaker : public QObject
{

    Q_OBJECT

public:

    Matchmaker(QObject *parent = Q_NULLPTR);
    void    setRequiredPlayerCount(int count);
    void    setSpawner(GameSpawn *spawn);
    virtual bool    checkCriterieas();

public slots:

    void    addPool(Client *client);

private:

    QQueue<Client*>  _pool;
    GameSpawn       *_spawner;
    int             _count;
    virtual void    matchmake();
    void            timerEvent(QTimerEvent *event);

};

#endif // MATCHMAKER_H
