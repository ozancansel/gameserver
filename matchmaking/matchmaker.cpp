#include "matchmaker.h"

Matchmaker::Matchmaker(QObject *parent) : QObject(parent)
{
    _count = -1;
    startTimer(1000 , Qt::CoarseTimer);
}

void Matchmaker::addPool(Client *client){

    //Eger kullanici zaten mac sirasinda ise
    if(_pool.contains(client)){
        qDebug() << client->name() <<  " zaten sirada...";
        return;
    }

    //Siraya ekleniyor
    _pool.enqueue(client);

    qDebug() << "MatchMaker::addPool() => Havuza eklendi. " << client->toString();
}

void Matchmaker::timerEvent(QTimerEvent *event){
    while(checkCriterieas()){
        QList<Client*>  clients;

        for(int i = 0; i < _count; i++){
            clients.append(_pool.dequeue());
        }

        //Spawning game

        qDebug() << "Oyun olusturuluyor.";
        _spawner->spawnGame(clients);
    }
}

bool Matchmaker::checkCriterieas(){
    return _pool.count() >= _count;
}

void Matchmaker::matchmake(){
    QList<Client*>  matchPlayers;
    for(int i = 0; i < _count; i++){
        if(_pool.isEmpty()){
            continue;
        }
        matchPlayers.append(_pool.dequeue());
    }

    _spawner->spawnGame(matchPlayers);
}

//Setter
void Matchmaker::setRequiredPlayerCount(int count){
    _count = count;
}

void Matchmaker::setSpawner(GameSpawn *spawn){
    _spawner = spawn;
}
