#ifndef GAMESERVER_H
#define GAMESERVER_H

#include "gameserver_global.h"
#include "client.h"
#include "matchmaking/matchmaker.h"
#include <QList>
#include <QObject>
#include <QTcpServer>
#include <QWebSocket>
#include <QWebSocketServer>

class GameServer : public QObject   {

    Q_OBJECT

public:

    GameServer(QObject *parent = Q_NULLPTR);

    void            listen(int port = 0);

private slots:

    void            onNewConnection();
    void            addMatchmaking(Games game);
    void            clientAuthorized();

signals:

    void            newConnection(Client *client);

protected:

    QWebSocketServer            *_server;
    QList<Client*>              _clients;
    QList<Client*>              _authorized;
    QHash<Games , Matchmaker*>  _matchmakers;

};

#endif // GAMESERVER_H
