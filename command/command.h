#ifndef COMMAND_H
#define COMMAND_H

#include <QObject>

class Command : public QObject
{

public:

    Command();
    void    parse(QString &ref);

};

#endif // COMMAND_H
