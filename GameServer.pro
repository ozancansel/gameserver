#-------------------------------------------------
#
# Project created by QtCreator 2017-07-09T10:43:53
#
#-------------------------------------------------

QT       += core network websockets

CONFIG   += console

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        gameserver.cpp \
    exception/portisoccupied.cpp \
    client.cpp \
    enum/games.cpp \
    matchmaking/matchmaker.cpp \
    spawn/gamespawn.cpp \
    command/command.cpp \
    main.cpp \
    spawn/twocarsspawn.cpp \
    game/twocars/twocarsgame.cpp \
    game/game.cpp \
    game/spaceship/spaceshipgame.cpp \
    spawn/spaceshipgamespawn.cpp

HEADERS += \
        gameserver.h \
        gameserver_global.h \ 
    exception/portisoccupied.h \
    client.h \
    enum/games.h \
    matchmaking/matchmaker.h \
    spawn/gamespawn.h \
    command/command.h \
    spawn/twocarsspawn.h \
    game/twocars/twocarsgame.h \
    game/game.h \
    game/spaceship/spaceshipgame.h \
    spawn/spaceshipgamespawn.h
