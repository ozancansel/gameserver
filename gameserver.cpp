#include "gameserver.h"
#include "spawn/twocarsspawn.h"
#include "spawn/spaceshipgamespawn.h"
#include "exception/portisoccupied.h"

GameServer::GameServer(QObject *parent) : QObject(parent)   {
    _server = new QWebSocketServer("matchmakingServer",QWebSocketServer::NonSecureMode);

    //Matchmakerlar olusturuluyor
    Matchmaker* twoCarMatchMaker = new Matchmaker(this);
    Matchmaker* spaceshipMatchMaker = new Matchmaker(this);
    GameSpawn* spawn = new TwoCarsSpawn(this);
    GameSpawn* spaceshipSpawn = new SpaceshipGameSpawn(this);

    //Parametreler ayarlaniyor
    twoCarMatchMaker->setRequiredPlayerCount(2);
    spaceshipMatchMaker->setRequiredPlayerCount(5);
    twoCarMatchMaker->setSpawner(spawn);
    spaceshipMatchMaker->setSpawner(spaceshipSpawn);

    //Oyunlar gore mapping yapiliyor
    _matchmakers[TwoCar]    =   twoCarMatchMaker;
    _matchmakers[SpaceShip] = spaceshipMatchMaker;

    connect(_server ,   SIGNAL(newConnection()) , this , SLOT(onNewConnection()));
}

void GameServer::listen(int port){
    bool listened = _server->listen(QHostAddress::Any, port);

    //Hata
    if(!listened){
        PortIsOccupied(_server->serverPort()).raise();
    }
    else{
        qDebug() << "Matchmaking Sunucusu " << _server->serverPort() << " unda baslatildi.";
    }
}

//Slots
void GameServer::onNewConnection(){
    QWebSocket *socket = _server->nextPendingConnection();
    Client *client = new Client(socket , this);

    //Ekleniyor
    _clients.append(client);


    connect(client , SIGNAL(authorized()) , this , SLOT(clientAuthorized()));
    //Clients'lara ekleniyor
    qDebug() << socket->peerAddress() << " baglandi.";
}

void GameServer::addMatchmaking(Games game){
    Matchmaker* maker = _matchmakers.value(game , Q_NULLPTR);

    //Matchmaker bulunamazsa geri donuluyor
    if(maker == Q_NULLPTR)
        return;

    Client* client =(Client*)QObject::sender();

    //Poola ekleniyor
    maker->addPool(client);
}

void GameServer::clientAuthorized(){
    Client *client = (Client*)QObject::sender();

    _authorized.append(client);
    connect(client , SIGNAL(addMatchMaking(Games)) , this , SLOT(addMatchmaking(Games)));
}
