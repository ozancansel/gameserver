#ifndef GAME_H
#define GAME_H

#include <QObject>
#include <QWebSocket>
#include <QWebSocketServer>
#include "client.h"

class Game : public QObject
{

    Q_OBJECT

public:

    explicit Game(QObject *parent = nullptr);
    virtual void onNewPlayer(Client *client);
    int     serverPort();
    bool    debugEnabled();
    void    setDebugEnabled(bool enabled);

signals:

public slots:

    virtual bool    startGameServer();
    void    authorized();

private slots:

    void    newPlayer();

private:

    bool            _debugEnabled;
    QWebSocketServer    *_server;
    QList<Client*>  _players;

};

#endif // GAME_H
