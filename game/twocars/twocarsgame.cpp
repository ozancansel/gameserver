#include "twocarsgame.h"
#include <QTimer>

TwoCarsGame::TwoCarsGame(QObject *parent) : Game(parent)  {
    _playerCount = 2;

    startTimer(16);

    _gameStateToStringMap[WAITING_EVERYONE_TO_CONNECT] = WAITING_EVERYONE_TO_CONNECT_STATE;
    _gameStateToStringMap[LOBBY_NOT_READY] = LOBBY_NOT_READY_STATE;
    _gameStateToStringMap[GAME_STARTING] = GAME_STARTING_STATE;
    _gameStateToStringMap[GAME_STARTED] = GAME_STARTED_STATE;
}

void TwoCarsGame::onNewPlayer(Client *client){

    connect(client , SIGNAL(onNewMessageReceived(QString,QStringList)) , this , SLOT(onNewMessageReceived(QString,QStringList)));

    PlayerState state;
    state.atLobby = true;
    _playerStateMap[client] = state;

    if(everyoneConnectedToServer()){
        if(debugEnabled())  qDebug() << "Herkes baglandi.";
        informPlayerNames();
    }
}

void TwoCarsGame::onNewMessageReceived(const QString &code, const QStringList &args){
    Client *client = (Client*)QObject::sender();

    if(code == SET_READY_COMMAND){
        QString arg = args.at(0);

        int read = arg.toInt();
        _playerStateMap[client].isReady = read;

        if(debugEnabled())
            qDebug() << QString("%0 is %1ready").arg(client->name()).arg(read ? QString("") : QString("not "));
    } else if(code == GAME_LOADED_COMMAND){
        _playerStateMap[client].gameLoaded = true;
        if(debugEnabled())   qDebug() << QString("%0 oyunu yukledi").arg(client->name());
    }
}

bool TwoCarsGame::startGameServer(){
    bool result = Game::startGameServer();

    qDebug() << "TwoCarsGame " << serverPort() << " nolu portta baslatildi.";

    return result;
}

bool TwoCarsGame::everyoneInLobby(){

    foreach (PlayerState state, _playerStateMap.values()) {
        if(!state.atLobby)  return false;
    }

    return true;
}

bool TwoCarsGame::everyoneReady(){

    foreach (PlayerState state, _playerStateMap.values()) {
        if(!state.isReady)  return false;
    }

    return true;
}

bool TwoCarsGame::everyoneLoadedGame(){
    foreach (PlayerState state, _playerStateMap.values()) {
        if(!state.gameLoaded)  return false;
    }

    return true;
}

bool TwoCarsGame::everyoneConnectedToServer(){
    return _playerCount <= _playerStateMap.keys().count();
}

void  TwoCarsGame::informPlayerNames(){
    if(debugEnabled())  qDebug() << "Kullanicilarla kullanici bilgileri paylasiliyor.";
    QString command(PLAYER_INFORMATIONS_COMMAND);
    foreach (Client *client, _playerStateMap.keys()) {
        command.append(QString(",%0").arg(client->name()));
    }

    broadcast(command);
}

void TwoCarsGame::changeState(GameState state){
    GameState   previousState = _state;

    //Lobbye gecilirken kullanici isimleri gonderiliyor
    if(previousState == WAITING_EVERYONE_TO_CONNECT && state == LOBBY_NOT_READY)
        informPlayerNames();

    _state = state;
    broadcast( QString("%0,%1").arg(GAME_STATE_CHANGED_COMMAND).arg(_gameStateToStringMap[state]));
}

void TwoCarsGame::broadcast(QString message){
    foreach (Client* player, _playerStateMap.keys()) {
        player->socket()->sendTextMessage(message);
    }
}

//Game Loop
void TwoCarsGame::timerEvent(QTimerEvent *event){
    switch (_state) {
    case WAITING_EVERYONE_TO_CONNECT:{
        bool connected = everyoneConnectedToServer();

        if(connected){
            changeState(LOBBY_NOT_READY);
        }
        break;
    }
    default:
        break;
    }
}
