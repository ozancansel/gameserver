#ifndef TWOCARSGAME_H
#define TWOCARSGAME_H

#include "game/game.h"
#include <QHash>
#define  WAITING_EVERYONE_TO_CONNECT_STATE      "0"
#define  LOBBY_NOT_READY_STATE                  "1"
#define  GAME_STARTING_STATE                    "2"
#define  GAME_STARTED_STATE                     "4"

//Out Commands
#define  PLAYER_INFORMATIONS_COMMAND            "12"
#define  GAME_STATE_CHANGED_COMMAND             "13"

//Incoming Commands
#define  SET_READY_COMMAND                      "10"
#define  GAME_LOADED_COMMAND                    "11"

class TwoCarsGame : public Game
{

    Q_OBJECT

public:

    TwoCarsGame(QObject *parent = Q_NULLPTR);
    virtual void    onNewPlayer(Client *client);

    typedef struct{
        bool    atLobby = false;
        bool    isReady = false;
        bool    gameLoaded = false;
    } PlayerState;

    enum    GameState{WAITING_EVERYONE_TO_CONNECT = 0,
                      LOBBY_NOT_READY = 1,
                      GAME_STARTING = 2,
                      GAME_STARTED = 3 };


public slots:

    void timerEvent(QTimerEvent *event);
    virtual bool startGameServer();
    virtual void onNewMessageReceived(const QString &code , const QStringList &args);

private:

    bool    everyoneConnectedToServer();
    bool    everyoneInLobby();
    bool    everyoneReady();
    bool    everyoneLoadedGame();
    void    broadcast(QString str);
    void    changeState(GameState state);
    void    informPlayerNames();
    QHash<Client* , PlayerState>    _playerStateMap;
    QMap<GameState , QString>       _gameStateToStringMap;

    int             _playerCount;
    GameState       _state;

};

#endif // TWOCARSGAME_H
