#ifndef SPACESHIPGAME_H
#define SPACESHIPGAME_H

#include "game/game.h"

class SpaceshipGame : public  Game
{

public:

    SpaceshipGame(QObject *parent = Q_NULLPTR);

    bool    startGameServer();
};

#endif // SPACESHIPGAME_H
