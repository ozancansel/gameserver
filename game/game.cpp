#include "game.h"

Game::Game(QObject *parent) : QObject(parent)
{
    _server = new QWebSocketServer("TwoCars" , QWebSocketServer::NonSecureMode );

    connect(_server , SIGNAL(newConnection()) , this , SLOT(newPlayer()));
}

void Game::newPlayer(){
    QWebSocket  *socket = _server->nextPendingConnection();
    Client  *client = new Client(socket , this);

    _players.append(client);

    connect(client , SIGNAL(authorized()) , this , SLOT(authorized()));
}

void Game::authorized(){
    Client* client = (Client*)QObject::sender();

    onNewPlayer(client);
}

void Game::onNewPlayer(Client *client){
    //Empty callback
}

bool Game::startGameServer(){
    bool listening = _server->listen();

    return listening;
}

int Game::serverPort(){

    if(!_server->isListening())
        return -1;

    return _server->serverPort();
}

void Game::setDebugEnabled(bool enabled){
    _debugEnabled = enabled;
}


bool Game::debugEnabled(){
    return _debugEnabled;
}
