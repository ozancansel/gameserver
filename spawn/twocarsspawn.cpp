#include "twocarsspawn.h"
#include "game/twocars/twocarsgame.h"

TwoCarsSpawn::TwoCarsSpawn(QObject *parent) : GameSpawn(parent)
{

}

Game*   TwoCarsSpawn::getGame(){
    Game* game = new TwoCarsGame(this);

    return game;
}
