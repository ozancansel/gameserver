#include "spaceshipgamespawn.h"
#include "game/spaceship/spaceshipgame.h"

SpaceshipGameSpawn::SpaceshipGameSpawn(QObject *parent) : GameSpawn(parent)
{

}

Game* SpaceshipGameSpawn::getGame(){
    return new SpaceshipGame(this);
}
