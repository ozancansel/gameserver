#ifndef SPACESHIPGAMESPAWN_H
#define SPACESHIPGAMESPAWN_H

#include "gamespawn.h"

class SpaceshipGameSpawn : public GameSpawn
{

public:

    SpaceshipGameSpawn(QObject *parent = Q_NULLPTR);
    Game*       getGame();
};

#endif // SPACESHIPGAMESPAWN_H
