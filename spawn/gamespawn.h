#ifndef GAMESPAWN_H
#define GAMESPAWN_H

#include <QObject>
#include "client.h"
#include "game/game.h"

class GameSpawn : public QObject    {

public:

    GameSpawn(QObject *parent = Q_NULLPTR);
    virtual bool    spawnGame(QList<Client*> clients);
    virtual Game*   getGame() = 0;

protected:

    void            addGameCollection(Game* game);

private:

    QList<Game*>    _spawnedGames;

};

#endif // GAMESPAWN_H
