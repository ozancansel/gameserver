#ifndef TWOCARSSPAWN_H
#define TWOCARSSPAWN_H

#include "gamespawn.h"

class TwoCarsSpawn : public GameSpawn
{

public:

    TwoCarsSpawn(QObject *parent = Q_NULLPTR);
    Game*       getGame();

};

#endif // TWOCARSSPAWN_H
