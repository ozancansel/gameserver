#include "gamespawn.h"

GameSpawn::GameSpawn(QObject *parent ) : QObject(parent)
{

}

bool GameSpawn::spawnGame(QList<Client *> clients){
    //Polymorphic fonksiyondan oyun aliniyor
    Game*   game = getGame();

    //Oyun sunucusu baslatiliyor
    bool serverStarted = game->startGameServer();

    //Server baslatilamadiysa
    if(!serverStarted)
        return false;

    int port = game->serverPort();
    QString url = QString("ws://localhost:").append(QString::number(port));

    //Oyunculara acilan serverin port ve ip numarasi gonderiliyor
    foreach (Client *client, clients) {
        client->informSpawnedGame(url);
    }
}
